import os
import gitlab
import requests
import json
from neo4j import GraphDatabase

from dotenv import load_dotenv
load_dotenv()

db_driver = GraphDatabase.driver(
	os.getenv('NEO4J_HOST'),
	auth=(os.getenv('NEO4J_USER'), os.getenv('NEO4J_PASS'))
)

def get_child_epic_ids(group_id, epic_id):
	api_endpoint = 'https://gitlab.com/api/v4/groups/%d/epics/%d/epics' % (group_id, epic_id)

	request_results = requests.get(api_endpoint, headers={'PRIVATE-TOKEN': os.getenv('GITLAB_API_TOKEN')})
	results = json.loads(request_results.text)
	return [x['iid'] for x in results]

gl = gitlab.Gitlab('https://www.gitlab.com', private_token=os.getenv('GITLAB_API_TOKEN'))
gl.auth()

group = gl.groups.get(9970) # main GitLab project

root_epic_id = 1359 # air-gap epic

def traverse_node_and_add_children(group_id, node, _type=None, _driver=None):

	with _driver.session() as neo4j_session:

		# Add this epic
		result = neo4j_session.run('''
			CREATE (n:Epic {
				id: %d,
				link: '%s',
				title: '%s',
				state: '%s'
			})
		''' % (node.iid, node.web_url, node.title, node.state))

		# Get all child issues for this epic
		child_issues = node.issues.list()

		# Add all child issues
		for issue in child_issues:
			result = neo4j_session.run('''
				CREATE (n:Issue {
					id: %d,
					link: '%s',
					title: '%s',
					state: '%s'
				})
			''' % (issue.iid, issue.web_url, issue.title, issue.state))
			print(result)

			result = neo4j_session.run('''
				MATCH (x:Epic), (y:Issue)
				WHERE x.id=%d AND y.id=%d
				CREATE (x)-[r:Child]->(y)
			''' % (node.iid, issue.iid))
			print(result)

		# Get all child epics for this epic
		child_epics = get_child_epic_ids(group_id, node.iid)

		# Loop through child epics
		for epic_id in child_epics:
			print (epic_id)
			the_epic = gl.groups.get(group_id).epics.get(epic_id)

			# Recurse on child epics. Each call will create the child nodes so don't add it here.
			traverse_node_and_add_children(group_id, the_epic, _driver=db_driver)

			# Add the link to the newly created child epics
			result = neo4j_session.run('''
							MATCH (x:Epic), (y:Epic)
							WHERE x.id=%d AND y.id=%d
							CREATE (x)-[r:Child]->(y)
						''' % (node.iid, the_epic.iid))
			print(result)

the_epic = group.epics.get(root_epic_id)

traverse_node_and_add_children(9970, the_epic, _driver=db_driver) # 9970 is the group_id
